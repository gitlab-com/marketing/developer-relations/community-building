# The DevOps Handbook: How to Create World-Class Agility, Reliabiity, and Security in Technology Organizations
Gene Kim, Jez Humble, Patrick DuBois, and John Willis

## Reading notes and summaries

### Introduction

> To understand the potential of the DevOps revolution, let us look at the manufacturing revolution of the 1980s. By adopting Lean principles and practices, manufacturing organizations dramatically improved plant productivity, customer lead times, product quality, and customer satisfaction, enabling them to win in the marketplace. (p. xxx)

Just as Lean principles and practices cut lead times for manufacturing, so too do they reduce the time typical for creaitng and deploying software innovations

> Now more than ever, how technology work is managed and performed predicts whether our organization will win in the marketplace or even survive. (p. xxxii)

IT organizations are always managing/balancing *change* and *statis* (consistency/stability); traditionally, Development "will take responsibility for responding to changes in the market" by developing something new and getting it into production as quickly as possible (p. xxxiii); conversely, Operations will "take responsibility for providing customers with IT service that is stable, reliable, and secure," and as change-resistant as possible (p. xxxiii); thus we have two opposing goals in tension that "creates a downward spiral so powerful it prevents the achievement of desired business outcomes" (p. xxxiii)

> DevOps is the outcome of applying the most trusted principles from the domain of physical manufacturing and leadership to the IT value stream. (p. 3)

DevOps' foundation is comprised of insights from:

* Lean
* Theory of Constraints
* Toyota Kata

> Many also view DevOps as the logical continuation of the Agile software journey that began in 2001. (p. 3)

### Chapter 1: Agile, Continuous Delivery, and the Three Ways



### Chapter 2: The First Way

Principles of Flow:

* Make our work visible
* Limit work in process
* Reduce batch sizes
* Reduce the number of handoffs
* Continually identify and elevate our constraints
* Eliminate hardships and waste in the value stream

> The First Way requires the fast and smooth flow of work from Development to Operations in order to deliver value to customers quickly. (p. 19)

Work must therefore become visible (using visualization techniques like, e.g., kanban). Work flows from left to right, and the team is focused on process improvements so it can do so "as quickly as possible" (p. 20).

> Work is not done when Development completes the implementation of a feature. Rather, it is only done when our application is running successfully in production, delivering value to the customer. (p. 21)

Work in process should be limited to encourage single-tasking the highest-priority work (i.e., place "an upper limit on the number of cards that can be in a column" on the visualization board).

Next, shirk the size of batches of work. We learn from Lean that "in order to shrink lead times and increase quality, we must strive to continually shrink batch sizes" (p. 23). Theoretical lower limit for size: single-piece flow.

Handoffs require communication and invariably lead to loss of context; each handoff puts resources in a queue where it waits for necessary resources to become available to complete it. Reducing handoffs therefore accelerates flow.

Flow always has a directionality, and therefore involves identifiable constraints. Any improvement *not* made at the point of contraint will not increase efficiency.

"Eliminating waste" in IT takes different meaning than in manufacturing: "reduce hardship and drudgery in our daily work through continual learning in order to achieve the organizaiton's goals" (p. 27); here "hardship" can be "anything that causes delay for the customer" (p. 27). These include things like partially done work, extra features, task switching, waiting, motion, etc.

Look for places where "heroics become necessary" and you'll likely find points of hardship (p. 29).




## Thoughts, impressions, connections
{This is where readers record their reactions to the reading, log key connections to other readings or to their own experiences, etc.}

## Discussion prompts
{This is where readers record statements or questions for synchronous group discussion.}

## Discussion notes

### 2023.04.05
Pj: Pj  
J: Jordan  
B: Bryan  

{Initial discussion of value streams an value stream mapping and GitLab's offering in this space.}

Pj: Analyzing the aopter "chasm" (the gulf between your ealry adopted and your more reluctant adopters); leaping across the chasm is difficult (thinking of early adopters in tech more broadly).

B: Always the toughest part of change management for people I work with.

J: Brings us to the next chapter, which is on incremental change and replicating success.

Pj: Same chapter that discusses intentional reduction of technical debt and the allocation of 20 percent of a team's regular time toward this end.

Bryan: 20 percent is a lot of time! But we practice this at GitLab by design (updating a handbook is a form of technical debt reduction).

J: And this work is in danger of never getting done, too (e.g., worked at a furniture store where an inventory project, delayed five years, just never completed).

Pj: Like the phrase "making interest payments on your technical debt"; really captures the sense of the amount of work that's truly necessary here

J: See also the section on greenfield v. brownfield projects. Brownfield projects aren't necessarily bad places to work! In either case, you're going to need to keep your goals defined and your time horizon short.

{Discussion of various value streams, value stream processes, and evaluation of value stream examples from case studies in the book.}




