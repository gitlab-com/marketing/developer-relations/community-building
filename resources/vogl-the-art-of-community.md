# The Art of Community: Seven Principles for Belonging
Charles H. Vogl

## Reading notes and summaries

### Preface

Book is intended "for those brave people who seek to bring others together to create something enriching, satisfying, and meaningful."

Contemporary research seems to indicate that "Americans today are seeking connection wither others who share their values," but "they're not involved in communities that typically provide deep ongoing connection, membership, and life-honoring rituals."

### Introduction

Communities "make us more effective in reaching goals and overcoming challenges large and small." They are "created when at least two people begin to feel concern for each other's welfare."

Book aims to help readers build communities in which members:

* grow in the ways they hope to
* feel more connected, welcome, proud, and excited to be part of the group
* work together toward making the difference that you envision
* have more fun

"Communities function best when they're helping members to be more successful in some way in a connected and dynamic world."

### Chapter 1: Understanding Community 
Definition of community: "a group of individuals who share a mutual concern for one another's welfare" (p. 9)

Features: 

* Shared values
* Membership identity
* Moral proscriptions
* Insider understanding

Morals in communities: what are the morals of a tech community? It's easier to see in something like FOSS, since the idea of charging for software is seen as morally wrong for many in that community. 

> One of the great pleasures of being part of a community is that we don't have to explain ourselves. We want to feel seen and understood without explaining the parts that outsiders don't get. We feel more comfortable and safer within the community because of this baseline understanding." (p. 26)

Example: a group of jazz musicians shortened their session time when an outsider was present (from a few hours to 30 minutes); it "changed the space and eroded the intimacy of the community time" (p. 27).

### Chapter 2: The Boundary Principle

Community members "want to know who's in the community and shares their values" (p. 33); all communities have boundaries that demarcate insiders from outsiders; these exist to help insiders feel safe and aid mutual recognition.

Boundaries can be explicit or implicit; either way, they are an unavoidable precondition for the formation of a community (community members have to be able to distinguish themselves from not-community members).

> It's imperative that the boundary is protected according to *community values*, as opposed to personal preferences, petty concerns, or whimsical criteria.

### Chapter 3: The Initiation Principle

Initiation is "any activity that's understood as official recognition and welcome into the community" (p. 43); it marks the transition from outer to inner ring (see previous chapter).

Should be clear, explicit, and immediately recognizable to others in the community; it's "a kind of ritual," (p. 44), so it involves symbols or tokens.

Initiation serves to help newcomers feel safe to explore and participate, too, clearly seeing that they haven't committed to anything or crossed a threshold they don't (yet) wish to cross).

Initiation often involves *invitation*, which can be a powerful tool for overcoming a "crisis of belonging" (when community members might begin to feel as though they don't belong, aren't members, or aren't valued); invitations themselves reinforce belonging regardless of whether they're accepted.

Pj: Noticed the "Accidental initiation" section. Where some kind of invitation exists, but it's not formally spelled out. But the initiate *knows* they're being initiated in some way. 

Pj: The Crisis of belonging! "...members become convinced they don't belong." Invitations from leaders, explicit "join us" type invitations, can allay these fears. Even if you don't have an official titel, the act of inviting can even bring the inviter deeper into the group. 

### Chapter 4: The Rituals Principle

> A ritual is any practice that marks a time of event as special or important. (p. 49)

Rituals have a temporal effect; they bridge past, present, and future in ways that create meaningfulness; "rituals are a tool to bring meaning into our lives" (p. 49)

Rituals might include:

* ritual silence 
* rites of passage
* community displays
* Play rituals

Foundation Form for Rituals: 
- Opening
    1. Welcome
    1. Intention
    1. Reference a Tradition
    1. Explain Events and Instructions
- Body
    1. Share Wisdom
    1. Invite Participation
- Closing
    1. Acknowledgment
    1. Sending


### Chapter 5: The Temples Principle

A temple is a "place where people with shared values enact their community's rituals" (p. 67); they are spaces set aside for purposes special to the community; "when you think about spaces that are special for you, you'll probably think about something that happens in that space that doesn't happen elsewhere" (p. 69)

Space affects ritual: Doing a ritual outside a designated space can be strange

Create sacred spaces for communities by:

* Drawing boundaries
* Inviting people to the space
* Donning special garments
* Adjusting the lighting
* Adjusting acoustics
* Elevating (physically) important aspects of the space and its rituals

### Chapter 6: The Stories Principle

Every community has stories; "sharing certain stories deepens a community's connections" (p. 75)

> If people don't know (or can't learn) your stories, they don't know or understand your community. They can't know who you are, what you do, or how what you do matters. (p. 75)


### Chapter 10: Managing Community Face-to-Face and Online

This chapter will cover three topic areas:

* Reasons/motivation for seeking success in/with community
* Applying the principles to online communities
* The work of Elinor Ostrom

#### Different kinds of success

> There's danger in inviting people with a selfish orientation to success into roles of authority. (p. 123)

People don't necessarily participate in or lead communities for purely altruistic reasons, but they do have different orientations toward gain:

* relative: seeking to do better than others ("relative to" others around us)
* personal maximization: seeking to do as best as one can regardless of others
* community maximization: seeking to maximize success for the group as a whole

#### Managing online communities

Online communities can overcome some limitations of space and time; they help members protect identities in cases where this can be important and enhance trust and disclosure; they make scaling a community easier; they enable recording and archving of principal community resources for use by new members

### Managing a Community

Elinor Ostrom's work on managing and growing communities, specifically with regard to long-term engagement and resource management, may be of interest here.

According to Ostrom, sustainable communities:

* have a clear group identity and boundaries
* have benefits proportional to costs
* make decisions together
* effectively monitor violators and/or free riders
* impose graduated sanctions on those who break community rules
* keep conflict management inexpensive and easy to access
* recognize the rights to organize
* coordinate with other relevant groups and networks

### Epilogue: Endings and Beginnings

Community dissolution is not necessarily an indication of failure; it could simply mean that the community has met its goals, served its purposes, or shifted its attention(s) elsewhere

Relationships we cultivate in our communities often outlast individual and specific communities themselves

## Thoughts, impressions, connections

* Defining community as shared values rather than interests is a great concept, but also one that makes me nervous when I consider the community we're trying to build in tech. What is the GitLab community if not a community of interest? I think writing down our community values (as opposed to company values) would go a long way in making sure we know what we're aiming for. There's some examples in chapter 1. 


## Discussion prompts

* Vogl's advice for creating sacred spaces seems to presume in-person ritualistic practice between co-present community members. How can we adapt this advice for distributed communities with remote and distanced members?
  * Pj: I think we can use what GitLab does best: Async and remote. Take the way we work at GitLab and apply that veneer to the advice in this book. TBH I looked to see if there was a "covid" update on this book, and there doesn't seem to be one. He does reference Twitch during chapter 5, though... followed by immediately noting "A well-built and well-managed online community can be great, but even at its best, it still doesn't provide the same high level of connection and feeling of belonging as meeting offline can"
  * It seems that GtiLab agrees, with the attempts to do in person Commit and Contribute (though delatyed as they are for safety) the company KNOWS in person is needed as well. 

* Is a forum/Discord/subReddit a Temple? Is there a "main" temple for developer/software communities? 

## Discussion notes

### 2022-12-07

Bryan wonders what resonated with Pj and what reinforced/contradicted education in/from C School. 

P: It's important to define your community. What your communities values are as well as what is excluded from your community. 

Anyone with a bike is welcome. "Are you sure?"

B: defining what you are not is part and parcel of defining what you are. It's true of us at every level.

The author's advice to be as explicit about what you aren't as much as what you are is good advice.

Our goals of just this reading group is a community. 

On the definition of community advanced here: Define welfare. This can be applied to work welfare. Grow in some way professionally, because we believe knowing and understanding GitLab is beneficial for our community. 

Definition of a thing is different from its functions. Community may do these things, but it's not what defines the community explicitly. 

B: We bandy the term "community" about in interesting ways; my housing community, my HOA, etc. I have people in my community whose welfare I care about but who I don't know. Adhering to symbols does not a community make. Community is emotional or affective in a way that is not captured with geographic location. There's a level of investment involved in it.

I could say that I'm investing in my local community by paying HOA fees, but does that mean I'm investing in the community? I care about the welfare of this community in terms of benefits and amenities, but can I do those *and* say I care *about* their welfare. 

Community  requires a shared identity.  This may be the difference between what we call a "program" and a "community." Is it a program or a community? What does it take to turn a program into a community?

pg. 15 "communities can have unhealthy implicit values without knowing it. They don't serve members..."

"When members are around other members the values and identity are reinforced..." 

Communities can't *not* have these things: Who i am, How I should act, What I believe: Its usually implicit and not articulated. Vogl's advice is that you should understand the answers or the archetypal person you could fill this out for. I don't see it as a template for community members to write, but you could extract these from other pieces. 

GitLab invokes its values often and zealously. I believe that well functioning communities they are: self reflective, adaptive, and constantly asking about their own purpose and well being. What allows them to say "Are we doing well, did we succeed or not succeed" are it's values. Are we operating in accordance with our values. 

Fatima: People will comment on our values (esp transparency) but we will see comments that showcase they have read our values and have adopted them or admire them. IT's cool and good to see. There's people who don't register with them; product focued commentary. There are values driven commentary. 

Pj: This helps us define who we say we're serving and whom we consider members of the community. Is it a descriptor, in vogls terms, of a community member. 

What do we expect of our community? is it contribution? 

B: There's a reason there's contributor success and Contribution program manager. There are other aspects of community outside of contribution. Is that the remit of our team? I don't think so. AS a marketing team, we're more aligned with marketing goals. 

CMO says we are awareness and evangelism. 

B: We can be the mouthpiece of the community. What separates us from *public* relations? We are also the mouthpiece of our communities *into* GitLab the company, as well. Advocating for our communities' needs as well.

### 2022-01-04

Jordan=J  
Pj=Pj  
Bryan=B  

Welcome to the group, Jordan!

J: I can use the language of this book to describe my Dungeons & Dragons party as a community

Pj: It may seem funny to describe these things as "rituals" and "temples" and "sacred spaces," but it totally makes sense and rings true

J: Seems like we tend to notice these things more when they've been violated

Pj: This certainly does make me reflect on my own participation in communities

J: Author says that in-person communities are "stronger"; is this true?

B: And what forms of power and privilege does a statement like that assume?

Pj: Was expecting more here on post-pandemic community, remote community, etc. But it's not here; the book was written pre-COVID

B: Thinking about community *and* GitLab versus community *on* GitLab and community *of* GitLab

J: When we say "communities" we're also really thinking about sub-communities

Pj: And in Community Relations we're always thinking about how we're reaching out to different/various communities that may or may not overlap; important to always understand *who* we're talking about when we say our team is serving "the community"

J: What about GitHub communities? When I talk to GitHub users, they seem nihilistic, jaded about the fact that Microsoft owns GitHub.

B: I wonder how GitHub would define "the GitHub community" and that community's role in GitHub's success; there are plenty of communities *using* GitHub but what is "the GitHub community" and its role?

Pj: This is why GitLab for Education is important: it builds community around GitLab, of passionate GitLab users

*We spend the rest of the time discussing the power of community invitations.*

## 2022-01-11

Jordan=J    
Pj=Pj  
Bryan=B  
Fatima=F

We open by discussing the tortured *Karate Kid* reference in Chapter 8. Jordan has never seen the movie. We scold her.

Okay we *really* begin by discussing stories and symbols. 

Diaconate: this is Vogl's term for people in the community that really understand the community, might not be "official" leaders, but are sources of knowledge and convention.

B: How do we architect pathways for diaconate to also mature, grow, and develop? This can be challenging when community members feel like they've achieved everything there is to achieve in the community.

Pj: And the community leaders aren't necessarily the longest-running members, either—or they needn't always be; however, people who are around the longest tend to assume this role.

F: Reminds me of my experience in a French language learning community

Pj: Parallels here to the GitLab Heroes program: heroes, superheros, core team members—ultimately becomes GitLab team member!

F: Get the sense that we have a strong inner ring, but a weaker outer ring—lots of people with long-term contributions and deep expertise, but not as easy for newcomers and casual contributors to get involved.

*Discussion of badges and badging and how this can assist.*

F: Drupal credit profiles are a nice example here; community really recognizes non-code contributions to the community and uses these to grow its contributor circles/rings

Pj: It's like leveling up in video games: eventually, you reach the top level (max level), and when others do, too, the pool becomes larger and less special, so game companies have to invent newer tiers or designations to help people remain motivated and achieve more exclusive status

*Discussion of various rings that constitute GitLab's community: community member, contributor, hero, superhero, core team member

B: Appreciated discussion of recognizing and being deliberate about identifying diaconate, people with outsized power and influence in the community; these always exist, and we should always be clear about who they are; these people should also own their status

F: The fear of abusing your power should not stop you from using your power

*Pj leads whiteboarding discussion of how we might visually map GitLab's community*
