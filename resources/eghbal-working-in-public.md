# Working In Public: The Making and Maintenance of Open Source Software
Nadia Eghbal

## Reading notes and summaries
<!-- This is where readers stores notes they take as they read—summaries, key quotations, etc. -->

### Introduction

Begin from the premise that contemporary open source developers suffer not from lack of project contributions but from *too many* of them.

> That many open source developers suffer from a lack of support is indisputable.

Eghbal initially focused on lack of funding for open source projects (cf. *Roads and Bridges*); "in the absence of additional reputational or financial benefits, maintaining code for general public use quickly becomes an unpaid job you can't quit"

> The cycle looks something like this: Open source developers write and publish their code in public. They enjoy months, maybe years, in the spotlight. But, eventually, popularity offers diminishing returns. If the value of maintaining code fails to outpace the rewards, many of these developers quietly retreat to the shadows. 

The goal of this work is to begin an analysis of open source production with different premises:

> The default hypothesis today is that, faced with growing demand, an open source "maintainer"—the term used to refer to the primary developer, or developers, of a software project—needs to find more contributors. [...] I started to see the problem is not that there's a dearth of people who want to contribute to an open source project, but rather that there are too many contributors—or they're the wrong kind of contributors. [...] But what if we start from the premise that things are as they should be? I decided to revisit the diagnosis, treating these symptoms as a starting point, instead of applying the only prescription—more participation—that we had.

A significant difference is that today most code is developed and deployed modularly; software projects aren't as monolithic as they once were:

> In contrast to big, monolithic software projects that give rise to persistent communities, npm packages are designed to be small and modular, which leads to fewer maintainers per project and a transitory relationship with the code they write. Viewed in this light, the lack of contributors today reflects an adaptation to changing environmental circumstances, wherein the relationship between a maintainer, contributors, and users is lighter, more transactional. 

This impacts the role of the maintain, too:

> The role of a maintainer is evolving. Rather than coordinating with a group of developers, these maintainers are defined by the need for curation: sifting through the noise of interactions, such as user questions, bug reports, and feature requests, which compete for their attention. [...] The problem facing maintainers today is not how to get more contributors but how to manage a high volume of frequent, low-touch interactions.

Analyses of various online platforms are also useful in this context, as platforms like GitHub (and GitLab) bring the same dynamics, logics, and economics to bear on the work of code creation:

> Rather than the users of forums or Facebook groups, GitHub's open source developers have more in common with solo creators on Twitter, Instagram, YouTube, or Twitch, all of whom must find ways to manage their interactions with a broad and fast-growing audience.

### Chapter 03: Roles, incentives, and relationships

This chapter examines the economics of open source production and distribution. Open source seems perplexing and counterintuitive because it contravenes so much received economic wisdom.

For example, Coase's theory of the firm:

> Previously, our understanding of how and why people make things was modeled after Ronald Coase's theory of the firm, which proposes that firms (i.e., companies, organizations, and other institutions with centralized resources) naturally emerge as a way to reduce transaction costs in the market. Coase would've told us that only companies make software because, from a coordination standpoint, managing the resources required to pull off such a feat would be most efficiently handled within the same organization.

Open source production seems to transcend traditional organizational boundaries, relations, or financial incentives. So another tack is to treat open source projects as commons (cf. Elanor Ostrom). This frame seems a more productive lens for understanding open source economically (that is, how they self-organize and self-govern to optimize resources).

> Ostrom's work on the commons helps us understand the conditions in which people produce software *collaboratively*: the clubs and federations of open source.

This model is (for critics like Benkler), a "third way" to organize and optimize resources: not a market, not a firm.

Community participants are often **intrinsically motivated** to engage in the work; "intrinsic motivation is the currency of the commons" (Eghbal); harnessing and channeling this energy/currency requires **modularity** and **granularity** (breaking large projets into smaller, more manageable pieces, and create discrete workstreams or projects without need for much preexisting knowledge). **Lower coordination costs** aid this (that is, lower costs of overhead, maintaining and orchestrating the work, quality control, integration, etc.).

> A maintainer's biggest coordination costs come from reviewing and merging new contributions, so there's an incentive to keep these costs low. When the costs of coordination outpace the benefits, the commons breaks down as a useful production model.

But not all open source projects really look or operate like a commons (e.g., "stadiums" as described in Chapter 02):

> Without the safety net of the commons, stadiums need to organize their work differently. Decentralized communities prioritize work based on *abundance* of attention: encouraging new contributors, developing governance processes, and improving engagement and retention. But a creator prioritizes work based on *scarcity* of attention: saying no to contributions, closing out issues, reducing user support. While the commons is tasked with resolving *coordination* issues, creators are defined by the need for *curation*.

Platforms like GitHub homogenize an otherwise variegated, heterogeneous federation of unconnected communities and spaces online; they produce a sense of "context collapse" (cf. Wesch)

> Today, most developers transact casually with one another, across projects, with low context and little skin in the game. These developments present a challenge to Ostrom's definition of a commons, and, by extension, to our current working theories about how and why open source software is produced.

Casual contributions to open source projects are today more commonplace, and the so-called "casual contributor" is a persona many open source projects and maintainers must determine how to handle.

> They only want to know enough to get their contribution merged. Their primary interest is personal, and it's the maintainer's job to figure out how to weigh the contributor’s needs against those of the project.

Casual contributors are *self-oriented* (they're working almost solely to fix something that's impacting or of interest to them) rather than *community-oriented*.

### Chapte4 04: The work required by software

Two observations guide this chapter:

* software is never really finished (may be feature-complete, but in order to keep running it almost always requires ongoing maintenance)
* once software finds users, it can't easily disappear (someone is going to use it for a long time)

## Thoughts, impressions, connections
<!-- This is where readers record their reactions to the reading, log key connections to other readings or to their own experiences, etc. -->

Bryan: Eghbal uses the term "developer" as a catch-all term for "people who've written and used code for some purpose in their lives," one that encapsulates other, more specific roles like "users," "contributors," and "maintainers" in open source projects. I wonder what folks think about this nomenclature, especially my colleagues on the Developer Relations team.

Pj (2023-07-12): This was an eye opening section (chapter 5 through page 176). Thinking of going up to the door of someone who has christmas lights and demanding the surfing santa made so much sense. However, when building an app or tool in open source, we rely on feedback to know what the community needs. I thought a good analogy is less knocking on the door and more putting a note in a suggestion box. 

Also, the idea that code is two separate goods (pg. 160) was interesting. It's production and consumption. 

Public v participatory seems an important consideration for any project. How much to allow the community access (read v write permissions) is important to consider. Someone showing up with a strange edge case and demanding that it be fixed by someone else vs a super user who also contributes to docs asking for a new feature are two very differently weighted asks. This is something I'm realizing we need to consider at my current company. Not every user must be satisfied to the fullest. 

- Jordan (2023-07-18): it would be interesting if GitLab partnered with universities to have students participate in open source contributions, similar to how in high school you have community service hours you have to complete to graduate.



## Discussion prompts
<!-- This is where readers record statements or questions for synchronous group discussion. -->

1. On page 9, Eghbal introduces the commonly held hypothesis that "faced with growing demand, an open source "maintainer" needs to find more contributors... It's commonly thought that open source software is built by communities..." Eghbal challenges this hypothesis with insight from maintainers and hazards "the problem is not that there's a dearth of people who want to contribute to an open source project, but rather that there are too many contributors--or they're the wrong kind of contributors." **In what ways have you observed a focus on quantity of contributors vs. quality of contributors and have you seen this have a negative impact on an open source project, e.g. GitLab?**

2. On page 14, Eghbal compares open source software developers to social media content creators: "Like any other creator, these developers create work that is intertwined with, and influenced by, their users, but it's not _collaborative_ in the way that we typically think of online communities... GitHub's open source developers have more in common with solo creators on Twitter, Instagram, YouTube, or Twitch, all of whom must find ways to manage their interactions with a broad and fast-growing audience...." **Do you agree with the analogy of open source developer and content creator? Do you think open source contributors are not "collaborative" within a community?**

3. "Both free and early open source advocates were preoccupied with evangelizing the idea of open source," Eghbal argues, "whether for ideological or business reasons. But today's developers hardly even notice 'open source' as a concept anymore." **Do you agree with this assessment? If so, what might be its implications—both for the contemporary development platform landscape and the future of open source development?**

## Discussion notes
<!-- This is a space for capturing notes during synchronous discussions. -->
### Session 1

- How is the work distributed more equally?
- The problem is no longer finding an audience but getting them to come back
- 1 off contributions does not a community make
- We use community to describe anybody who does anything related to GitLab
- The crisis in open source is not a lack of contributions but lack of maintainers
- A collaborative project is one where the community helps set the goals, the project
- Collaboration vs cooperation
- Community board/counsel

### Session 2

- Steve Klabnik: [What comes after open source?](https://steveklabnik.com/writing/what-comes-after-open-source) and [The culture war at the heart of open source](https://steveklabnik.com/writing/the-culture-war-at-the-heart-of-open-source)

### Session 3

- Initial discussion of the book's conclusion and its import
- "A paywall is more like the ticket kiosk at a theme park than a price tag on a car."
- Conclusion is more about applicability of lessons from open source to the social media/content creation industry landscape
- We do agree that the phenomenon Edhbal describes here is salient and pertinent today
- "Historically, most of our questions about the value of content have focused on the distribution side, rather than the production side. Today, the most interesting questions we can ask will focus on how content is made and maintained, and by whom."
- 

## Last updated
2023-07-26
