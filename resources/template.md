# Title of the work
Author of the work

## Reading notes and summaries
<!-- This is where readers stores notes they take as they read—summaries, key quotations, etc. -->

## Thoughts, impressions, connections
<!-- This is where readers record their reactions to the reading, log key connections to other readings or to their own experiences, etc. -->

## Discussion prompts
<!-- This is where readers record statements or questions for synchronous group discussion. -->

## Discussion notes
<!-- This is a space for capturing notes during synchronous discussions. -->

## Last updated
