# Get Together
Bailey Richardson, Kevin Huynh, and Kai Elmer Sotto

## Reading notes and summaries

### Chapter 01

Begin with a discussion of the concept of community:

> Today, the meaning of "community" can be ambiguous. But true communities are simply groups of people who keep coming together over what they care about. The most vibrant communities offer members a chance to act on their passions with one another.

An underlying premise of this book:

> The secret to getting people together is this: build your community with people, not for them.
 
> In other words, approach community-building as progressive acts of collaboration—doing more with others every step of the way.

The first chapter concerns the art of building your community's "core," or "kindling":

> If you want to spark your own community, you’ll need to first pinpoint your people. Find your kindling—those early allies who care about what you care about enough to manifest your idea for a community into an actual gathering of human beings. Though there may not be many of them, the first people you involve are consequential. They will set the tone and direction for the future of your group.
 
> Building a community isn’t about you and what you can do; rather, it’s dependent upon what you and your people can do.

One example is Twitch:

> Justin.tv doubled down on cultivating a community of gamers when the founders examined who was most engaged and then aligned the mission of their business with that group’s motivations.

> Like most relationships, communities don’t form overnight; they take time to flourish. You’ll need to stay invested in these people if you want to bring a community to life.
 
> The people who care are more powerful than the people who don’t. They alone will help you build a community from scratch.
 
> You will need to set the tone with your early group if you want to attract and welcome new types of people. Keep that responsibility at the forefront as you rally, nurture, and evolve your community.

### Chapter 02

This chapter concerns the art of building commuity through engagement in joint activities.

> Communities form around shared activities. Sometimes the activity is impossible to do alone. Other times the activity is fine solo but 10 times better when done with others.

Engaging in *joint* activities is a means of discovering shared purpose:

> Members realize their community's purpose through the thing that they do together. In other words, kindred spirits operating in silos aren't a community (yet).

Community architects should be deliberate and thoughtful in their design of initial, shared activities for community members:

1. Make it purposeful
1. Make it participatory
1. Make it repeatable

Communities must be *engaged*; working with them is different than working with, e.g., an "audience":

> Stop thinking about your community as just an audience. Instead, treat these people as collaborators.

This is what it means to create something *participatory*.

Event should also be *repeatable*. At the heart of community is shared ritual, which involves repeat activities:

> Every thriving community organizes essential, repeating activities for its members. If communities are about people coming together, one of the most important things you can do is create ways for them to keep coming together. Repeating activities set the stage for members to deepen their relationships and for the community to gain momentum.

A key to building communities is to find those activities in which joint engagement allows people to achieve something they couldn't achieve alone:

> If what you do as a group amplifies what members experience alone, you’re on your way to sparking a community.

## Thoughts, impressions, connections

Bryan: Reading Chapter 2 reminded me of an article I helped publish when I was working for Opesource.com: [How I learned the difference between a community and an audience](https://opensource.com/open-organization/15/10/how-i-learned-difference-between-community-and-audience)

## Discussion prompts
<!-- This is where readers record statements or questions for synchronous group discussion. -->

## Discussion notes

### 2024-01-17
Discussion of Chapter 01

- the first chapter goes over how to identify the kindling for your community and how to build _with_ them, not _for_ them 
- the book also provides some activites and worksheets that can help you apply your context
- we're not sure that we've intentionally done exercies in our gitlab community in this way
- the book presents a good model for how to build a community in a for-profit company
- as a company, we might feel that the only form of value in a community is money exchange, but that lens is not always the most helpful way to look at community contributions
- [Taxonomy of open value exchanges](https://gitlab.com/bbehr/gitlab-notes/-/blob/main/taxonomy-open-value-exchanges.md)
  - this taxonomy helps you understand the things people do in non-monetary contexts and how a company needs to acknowledge and respect those in ways that helps them build community
  - more than just swag; there are other ways to drive value

## Last updated
2024-01-24

