## Selection details

* **Title:** 
* **Author(s)**: 
* **Informational link**: 

## Accessibility

<!-- Explain means by which the selection is available—e.g., as an open or gated download, as a paperback, in both print or audio, etc.—and provide links to those formats. -->

This book is available as:

* 
* 
* 

## Rationale for proposing

<!-- Briefly explain why the group might wish to read and study this material. How does it enhance our knowledge of community building principles and practices? What challenges does it help the group address? What do you hope the group will take away from it? -->

/label ~"Reading Group::Proposed"
